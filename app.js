// import du module http
const http = require('http');
const url = require('url');
const queryString = require('querystring');

const students = [
    { id: 1, firstname: "Héloïse", lastname: "Andelhofs", gender: 'f' },
    { id: 2, firstname: "Timothy", lastname: "Beek", gender: 'm' },
    { id: 3, firstname: "Mathilde", lastname: "Bouckaert", gender: 'f' },
    { id: 4, firstname: "Romain", lastname: "Bourgeois", gender: 'm' },
    { id: 5, firstname: "Jean-Christophe", lastname: "Cartiaux", gender: 'm' },
    { id: 6, firstname: "Tom", lastname: "Colin", gender: 'm' },
    { id: 7, firstname: "Nicolas", lastname: "Gemine", gender: 'm' },
    { id: 8, firstname: "Tony", lastname: "Jadoulle", gender: 'm' },
    { id: 9, firstname: "Vanessa", lastname: "Lutgen", gender: 'f' },
    { id: 10, firstname: "Alain", lastname: "Roos", gender: 'm' },
    { id: 12, firstname: "Kevser", lastname: "SARUHAN", gender: 'f' },
    { id: 11, firstname: "Maxime", lastname: "Turla", gender: 'm' },
]

// création du serveur
const server = http.createServer((req, res) => {
    //req -> La requête que reçoit notre serveur
    //res -> La réponse que va envoyer notre serveur
    console.log(req.url);
    //req.url -> récupère tout ce qui est écrit dans l'url de requête après le nom du serveur, donc ici, après localhost:8080

    //url.parse(uneUrl) -> Récupère un objet, avec toutes les parties découpées
    const requestUrl = url.parse(req.url).pathname; //route
    const requestQuery = url.parse(req.url).query; //options passées avec ?
    const requestMethod = req.method; //Get ou Post

    console.log("Path : ", requestUrl);
    console.log("Query : ", requestQuery);
    console.log("Method : ", requestMethod);

    //Si on est sur localhost:8080/
    if (requestUrl === '/' && requestMethod === 'GET') {
        //On peut setup la réponse que notre serveur va donner
        res.writeHead(200); //Envoyer un code pour savoir si la requête est un succès, une erreur etc
        // res.end('Coucou');
        //Envoyer un contenu à gérer (texte, html, json, fichier etc)
        // res.end('<h1>Coucou</h1>'); 
        res.end(`
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
            <style> 
                h1 {
                    color : pink;
                }
            </style>
        </head>
        <body>
            <h1>Bienvenue à la formation Web Apps 💻</h1>
            <h2>Nous sommes le ${new Date().toLocaleDateString('fr-BE')}</h2>
            <a href="/students">Découvrez la liste des étudiants</a>
        </body>
        </html>
        `)

    }
    //Si on est sur localhost:8080/students/
    else if (requestUrl === '/students') {
        if (requestMethod === 'GET') {
            console.log('Requête sur students');
            //Si on a une query
            if (requestQuery) {
                console.log("Query : ", requestQuery);
                //On la découpe
                const query = queryString.parse(requestQuery);
                //Si c'était l'id
                if (query.id) {
                    console.log('Présence d\'un id dans la query');
                    // On cherche le student dont l'id correspond à celui de la query
                    // renvoie undefined si pas trouvé
                    let studentToFind = students.find(stu => stu.id == query.id);
                    // On teste si on a réussi à récupérer un student
                    if (studentToFind) {
                        console.log("Etudiant trouvé");
                        //On affiche la page avec le student
                        res.writeHead(200);
                        res.end(`
                        <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <title>Document</title>
                            <style>
                                body {
                                    background-color : ${studentToFind.gender == 'm' ? 'lightblue' : 'pink'}
                                }
                            </style>
                        </head>
                        <body>
                            <h1>Voici les informations de l'étudiant n°${studentToFind.id} </h1>
                            <h2>${studentToFind.gender == 'm' ? 'Il' : 'Elle'} s'appelle ${studentToFind.firstname} ${studentToFind.lastname}<h2>
                            <a href="/students" >Revenir à la liste des étudiants</a>
                        </body>
                        </html>
                        `);
                    }
                    else {
                        console.log("Etudiant non trouvé");
                        res.writeHead(200);
                        res.end(`
                        <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <title>Document</title>
                        </head>
                        <body>
                            <h1>L'étudiant n°${query.id} n'existe pas</h1>
                            <a href="/students" >Revenir à la liste des étudiants</a>
                        </body>
                        </html>
                        `)
                    }
                }
                //Si c'était le genre
                if (query.gender) {
                    console.log('Gender détecté');
                    //Si on a tapé n'importe quoi dans la query gender
                   if (query.gender == 'm' || query.gender == 'f') {

                        let lis = '';
                        res.writeHead(200);
                        res.end(`
                    <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>Document</title>
                        <style>
                                body {
                                    background-color : ${query.gender == 'm' ? 'lightblue' : (query.gender == 'f') ? 'pink' : 'lightgrey'}
                                }
                            </style>
                    </head>
                    <body>
                        <h1>Voici la liste des étudiants pour cette formation 👨‍👨‍👧‍👦</h1>
                        <div>
                            <a href="/students">Revenir à la liste des étudiants</a>
                        </div>
                        <ul>
                            <!-- ajout avec la version où on utilise le reducer pour faire la liste -->
                            ${students.filter(stu => stu.gender == query.gender).reduce((lis, student) => lis + `<li><a href="/students?id=${student.id}"> ${student.firstname} ${student.lastname}</a> </li>`, '')}
                        </ul>
        
                        <h2> Ajouter un nouvel étudiant : </h2>
                        <form method="post">
                            <div>
                                <label for="lastname">Nom :</label>
                                <input id="lastname" name="lastname" type="text" placeholder="Ex : Dupont">
                            </div>
                            <div>
                                <label for="firstname">Prénom :</label>
                                <input id="firstname" name="firstname" type="text" placeholder="Ex : Jacques">
                            </div>
                            <div>
                                <input type="submit" value="Ajouter">
                            </div>
                        </form>
                    </body>
                    </html>
                    `);                   
                }
                else {
                    res.writeHead(404);
                    res.end(`
                    <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <title>Document</title>
                        </head>
                        <body>
                            <h1>Ce genre n'existe pas</h1>
                            <a href="/students" >Revenir à la liste des étudiants</a>
                        </body>
                        </html>
                    `);
                }
            }
            }
            else {

                let lis = '';
                for (const student of students) {
                    lis += `<li> ${student.firstname} ${student.lastname} </li>`;
                }

                res.writeHead(200);
                res.end(`
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Document</title>
            </head>
            <body>
                <h1>Voici la liste des étudiants pour cette formation 👨‍👨‍👧‍👦</h1>
                <div>
                    <a href="/">Revenir à la page d'accueil</a>
                </div>
                <div>
                    <a href="/students?gender=m">Afficher les hommes</a>
                    <a href="/students?gender=f">Afficher les femmes</a>
                </div>
                <ul>
                    <!-- ajout avec la version où on a fait à la main la liste -->
                    <!-- ${lis} -->

                    <!-- ajout avec la version où on utilise le reducer pour faire la liste -->
                    ${students.reduce((lis, student) => lis + `<li><a href="/students?id=${student.id}"> ${student.firstname} ${student.lastname}</a> </li>`, '')}
                </ul>

                <h2> Ajouter un nouvel étudiant : </h2>
                <form method="post">
                    <div>
                        <label for="lastname">Nom :</label>
                        <input id="lastname" name="lastname" type="text" placeholder="Ex : Dupont">
                    </div>
                    <div>
                        <label for="firstname">Prénom :</label>
                        <input id="firstname" name="firstname" type="text" placeholder="Ex : Jacques">
                    </div>
                    <div>
                        <label for="gender">Genre :</label>
                        <select name="gender" id="gender">
                            <option value="m">Homme</option>
                            <option value="f">Femme</option>
                        </select>
                    </div>
                    <div>
                        <input type="submit" value="Ajouter">
                    </div>
                </form>
            </body>
            </html>
            `)
            }
        }
        if (requestMethod === 'POST') {
            console.log("formulaire envoyé");

            let formData = '';
            //On va ajouter un eventListener
            // "à l'envoi de datas"
            req.on('data', (form) => {
                //Tant qu'on sait lire des datas, on remplit notre variable forData avec ce qu'on a pu lire
                formData += form.toString('utf-8');
            })
            //On ajoute un eventListner sur l'arrêt de la lecture de datas
            req.on('end', () => {
                console.log(formData);
                //J'obtiens, quelque chose comme ça
                //lastname=Pouet&firstname=Pouet&gender=m
                //queryString est un module, qui nous permet de découper une query, partout où il y a un &, il fait ensuite un objet
                const studentToAdd = queryString.parse(formData);
                console.log(studentToAdd);
                //On vérifie que la personne a bien rempli le formulaire
                if (studentToAdd.firstname.trim() !== '' && studentToAdd.lastname.trim() !== '') {
                    //rajouter l'id
                    //Math.max(1,45, 85, 2, 6)
                    //Math.max accepte plusieux séparés par des , mais pas un tableau
                    //students.map(stu => stu.id) on transforme le tableau d'obj student en un tableau d'id -> [1, 2, 3, 4,...,12]                    
                    studentToAdd.id = Math.max(...students.map(stu => stu.id)) + 1;
                    //rajouter le student dans la liste
                    if(requestQuery === 'gender=m') {
                        studentToAdd.gender = 'm';
                    }
                    if(requestQuery === 'gender=f'){
                        studentToAdd.gender = 'f';
                    }
                    students.push(studentToAdd);
                }

                //après envoi du formulaire, on redirige vers une autre page
                res.writeHead(301, {
                    location: '/students'
                }); //En gros, va rafraichir la page, pour refaire l'affichage de la liste et surtout qu'on y soit en GET et plus en POST
                res.end();

            })
        }
    }
    else {
        //si c'est ni la route de base, ni la route students 
        res.writeHead(404);
        res.end(`
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Document</title>
                <style>
                    img {
                        width : 800px;
                    }
                </style>
            </head>
            <body>
                <h1>404 - La page que vous demandez, n'existe pas ⚠</h1>
                <div>
                    <a href="/">Revenir à la page d'accueil</a>
                </div>

                <img src="https://static.vecteezy.com/system/resources/previews/004/639/366/non_2x/error-404-not-found-text-design-vector.jpg" alt="erreur 404" />
            </body>
            </html>
            `);
            return;
    }

})

// 'écouter' notre serveur -> Le rendre disponible sur un port
server.listen(process.env.PORT, () => {
    console.log(`Server started on Port ${process.env.PORT}!`);
})

// En API
// Method :
// Get : Récupérer des données
// Post : Envoi de données
// Patch & Put : Modification de données
// Put : Remplace tout l'objet modifié par le nouveau
// Patch : Remplace juste les données qu'on veut
// Delete : Suppression

