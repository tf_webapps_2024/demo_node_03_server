# Création d'un serveur Node :
## Initialiser le projet
* Initialiser le projet avec node
```
    npm init
```
* (Optionnel -> Seulement si mis sur git -> créer le fichier gitignore)
* (Optionnel -> Seulement si mis sur git -> initialiser le projet comme un repo git -> git init)
* Créer le fichier entry point (app.js ou index.js)
* Rajouter dans le package.json la commande start dans les scripts
```json
    "start" : "node app.js",
```

## Création du serveur avec le module http
```js
    // import du module http
    const http = require('http');

    // création du serveur
    const server = http.createServer((req, res) => {
        console.log(req);
    })

    // 'écouter' notre serveur -> Le rendre disponible sur un port
    server.listen(8080, () => {
        console.log(`Server started on Port 8080 !`);
    })
```

## Utilisation de nodemon
Une fois le serveur lancé avec la commande **node app.js**, si on modifie le code, on doit relancer à chaque fois la commande pour que tout soit recompiler et que les modifications soient bien prises en compte.
Ce serait bien de pouvoir faire comme avec Live Server (quand vous faisiez du JS Vanilla html, css, js), où, quand je modifie mon code, ça recompile automatique et je dois mes modifications en temps réel sans avoir à relancer la commande **node app.js** à chaque fois.
Pour ça, on a le module nodemon, qui entre en jeu !
* Installer nodemon dans le projet node
 !! Attention, c'est une dépendance de Dev, mon projet n'en a pas besoin pour fonctionner, c'est moi qui en ai besoin pour m'aider à dev.
```
    npm i -D nodemon
```
* Ceci va installer le module nodemon dans le projet (dans node_modules) et en passant, va l'ajouter aux dépendances dev du projet pour que la prochaine fois que quelqu'un tapera **npm i** dans le projet, ça aille chercher toutes les dépendances et les installe.
* Rajouter dans les scripts du package.json
```json
    "dev" : "nodemon app.js",
```
* Pour lancer le serveur avec nodemon
```
    npm run dev
```

## Utilisation des variables d'environnement
* Créer un fichier .env (qui ne devra pas se retrouver sur git, on y met des infos sensibles)
```
    PORT=NumeroDuPort
```
* Utilisation de la variable dans le fichier app.js
```js
    server.listen(process.env.PORT, () => {
        console.log(`Server started on Port ${process.env.PORT}!`);
    })
```
* Problème : Rien n'a mis les variables d'environnement créées dans le fichier .env dans mes variables d'environnement
* Depuis la version 20.x.x de node, il y a une solution présente en node pour le faire, on doit juste rajouter notre fichier d'environnement, dans la commande pour lancer le projet
```json
    "dev" : "nodemon --env-file=.env app.js",
```
* Avant la version 20.x.x de node, je devais rajouter un module qui récupérait tous les fichiers .env et chargeait le contenu dans les variables d'environnement : https://www.npmjs.com/package/dotenv
```
    npm install dotenv
```
On devait rajouter ensuite en première ligne du app.js
```js
    require('dotenv').config(); //prend tous les fichiers .env et setup les variables d'environnement
```

## Req & Res
Quand on créé le serveur, on lui passe une fonction, qui reçoit toujours :
* En premier paramètre : La requête que le serveur reçoit
* En deuxième paramètre : La réponse que le serveur devra renvoyer
```js
    const server = http.createServer((req, res) => {
        //req : requête
        //res : response
    })
```