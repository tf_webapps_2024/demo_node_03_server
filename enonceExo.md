# Grâce à la query qu'on n'a pas utilisé et queryString :
Rappel :
requestUrl -> /students/
requestQuery -> Tout ce qu'il y a après le ?

--------------
## Si la route est /students?id=X
Afficher une page avec toutes les infos de la personne dont l'id a été renseigné (sans le formulaire d'ajout)
Si son genre est masculin, page bleu, si féminin page rose
Si l'id n'est pas dans la liste -> redirection vers 404

--------------
## Si la route est /students?gender=m
Afficher uniquement les students masculins avec un fond bleu
Le formulaire n'a pas le select et fera d'office la création d'un student masculin

--------------
## Si la route est /students?gender=f
Afficher uniquement les students feminins avec un fond rose
Le formulaire n'a pas le select et fera d'office la création d'un student féminin